# Deprecation Notice

No longer maintained. I have finally decided to embrace the tmux (and you should too).

# What?

Adds a vim command for interacting with harness (see https://gitlab.com/rvaiya/harness).


# Installation

## Using pathogen..

```
cd ~/.vim/bundle
git clone http://gitlab.com/rvaiya/vim-harness
```

# Usage

First launch harness.

- E.G
    `harness ~/.harness`
- Put something like the following in ~/.vimrc `let harness_file='~/.harness'`

The Harness command should now be available for sending 
commands to the active harness instance.  

```:Harness ls```


function! harness#run(cmd, dir)
    let hf = expand(g:harness_file)
    if !filereadable(hf)
        echoerr hf." does not exist"
        return
    endif

    let cmd = 'cd "'.a:dir.'";'.a:cmd
    call writefile([cmd], hf)
endfunction

function! HarnessLast()
    call harness#run(g:harness_last_cmd, getcwd())
endfunction

function! Harness(cmd)
    let cmd = substitute(a:cmd, '%', expand('%'), 'g')
    let g:harness_last_cmd = cmd
    call harness#run(cmd, getcwd())
endfunction

let harness_file='~/.harness'
command! -nargs=1 -complete=shellcmd Harness call Harness('<args>')
command! -nargs=0 HarnessLast call HarnessLast()
